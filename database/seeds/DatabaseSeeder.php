<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        $this->call('UserTableSeeder');
    }

}

class UserTableSeeder extends Seeder
{

    public function run()
    {
        DB::table('users')->delete();

        for ($n = 0; $n <= 10000; $n++) {
            User::create(['email' => 'gordon+' . $n . '@murrion.com', 'password' => Hash::make('password_' . $n)]);
        }
    }

}