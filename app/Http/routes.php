<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

class User extends Illuminate\Database\Eloquent\Model
{

}

// simple homepage
$app->get('/', function () use ($app) {

    return 'Hello';
});

// add a new user manually
$app->get('/new', function () use ($app) {

    $user = new User;

    $user->email = 'gordon@murrion.com';

    $user->save();

    return 'All DOne';
});

// show a specific user
$app->get('/user/{id}', function ($id) {

    return User::findOrFail($id);
});


// log in a user
use Illuminate\Http\Request;

$app->post('auth/login', function (Request $request) {

    if (Auth::attempt($request->only('email', 'password'))) {
        echo 'Logged in successfully';
    } else {
        echo 'Failed to log in';
    }

});